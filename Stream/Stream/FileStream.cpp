#include "stdafx.h"
#define _CRT_SECURE_NO_WARNINGS
#include "FileStream.h"

namespace msl {
	FileStream::FileStream(const char* fileName)
	{
		_file = fopen(fileName, "w");
	}

	FileStream::~FileStream()
	{
		fclose(_file);
		delete _file;
	}

	FileStream& FileStream::operator<<(const char *str)
	{
		OutStream::operator<<(str);
		return *this;
	}

	FileStream& FileStream::operator<<(int num)
	{
		OutStream::operator<<(num);
		return *this;
	}

	FileStream& FileStream::operator<<(void(*pf)(FILE*))
	{
		OutStream::operator<<(pf);
		return *this;
	}
}