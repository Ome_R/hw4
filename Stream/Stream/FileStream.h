#pragma once

#include "OutStream.h"

namespace msl {
	class FileStream : public OutStream {
	public:
		FileStream(const char* fileName);
		~FileStream();

		FileStream& operator<<(const char *str);
		FileStream& operator<<(int num);
		FileStream& operator<<(void(*pf)(FILE*));
	};
}