#pragma comment(lib, "../Debug/Stream.lib")
#include "../Stream/FileStream.h"
#include "../Stream/OutStream.h"
using namespace msl;

int main() {
	OutStream* stream = new OutStream();

	*stream << "Test" << endline;

	delete stream;

	getchar();

	return 0;
}