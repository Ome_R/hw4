#include "Logger.h"

void Logger::setStartLine()
{
	static unsigned int counter = 0;
	_startLine = false;
	*os << "[LOG #" << ++counter << "] ";
}

Logger::Logger()
{
	os = new OutStream();
	_startLine = true;
}

Logger::~Logger()
{
	delete os;
}

Logger & operator<<(Logger & l, const char * msg)
{
	if (l._startLine)
		l.setStartLine();
	*(l.os) << msg;
	return l;
}

Logger & operator<<(Logger & l, int num)
{
	if (l._startLine)
		l.setStartLine();
	*(l.os) << num;
	return l;
}

Logger & operator<<(Logger & l, void(*pf)(FILE*))
{
	l._startLine = true;
	*(l.os) << pf;
	return l;
}
