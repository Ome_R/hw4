#pragma once

#include <stdio.h>

class OutStream
{
public:
	OutStream();
	~OutStream();

	OutStream& operator<<(const char *str);
	OutStream& operator<<(int num);
	OutStream& operator<<(void(*pf)(FILE*));
protected:
	FILE* _file;
};

void endline(FILE* file);