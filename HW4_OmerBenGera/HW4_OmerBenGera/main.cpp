#define _CRT_SECURE_NO_WARNINGS

#include "OutStream.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "Logger.h"
#include <stdio.h>

int main()
{
	char* test = "5";
	OutStream* outStream = new OutStream();

	//A
	*outStream << "I am the Doctor and I�m " << 1500 << " years old" << endline;

	//B
	FileStream* fileStream = new FileStream("test.txt");

	*fileStream << "I am the Doctor and I�m " << 1500 << " years old" << endline;

	//C
	OutStreamEncrypted* encrypted = new OutStreamEncrypted(3);
	*encrypted << "I am the Doctor and I�m " << 1500 << " years old" << endline;

	//D+E
	Logger* logger1 = new Logger();
	Logger* logger2 = new Logger();
	*logger1 << "First log" << endline;
	*logger2 << "Second log" << endline;
	*logger1 << "Third log" << endline;
	*logger2 << "Fourth log" << endline;

	delete outStream;
	delete fileStream;
	delete encrypted;
	delete logger1;
	delete logger2;

	getchar();

	return 0;
}
