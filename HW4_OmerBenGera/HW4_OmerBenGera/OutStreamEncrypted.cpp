#define _CRT_SECURE_NO_WARNINGS

#define MIN_ASCII 32
#define MAX_ASCII 126

#include "OutStreamEncrypted.h"
#include <stdlib.h>
#include <string.h>

OutStreamEncrypted::OutStreamEncrypted(int heist)
{
	_heist = heist;
	_file = stdout;
}

OutStreamEncrypted & OutStreamEncrypted::operator<<(const char * str)
{
	char* buffer = nullptr;

	encrypt(&buffer, str);

	OutStream::operator<<(buffer);

	delete buffer;

	return *this;
}

OutStreamEncrypted & OutStreamEncrypted::operator<<(int num)
{
	char buffer[12];
	sprintf(buffer, "%d", num);
	operator<<(buffer);
	return *this;
}

OutStreamEncrypted & OutStreamEncrypted::operator<<(void(*pf)(FILE *))
{
	OutStream::operator<<(pf);
	return *this;
}

void OutStreamEncrypted::encrypt(char** buffer, const char* str) const
{
	int length = strlen(str);
	*buffer = (char*) malloc(sizeof(char) * (length + 1));
	(*buffer)[length] = '\0';
	int currentChar;

	for (int i = 0; i < length; i++) {
		currentChar = (int) str[i];
		currentChar = ((currentChar + _heist) % MAX_ASCII);
		if (currentChar < MIN_ASCII)
			currentChar += MIN_ASCII - 1;
		(*buffer)[i] = (char)currentChar;
	}
}
