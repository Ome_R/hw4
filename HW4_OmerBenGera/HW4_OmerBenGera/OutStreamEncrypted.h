#pragma once

#include "OutStream.h"

class OutStreamEncrypted : public OutStream {
private:
	int _heist;
public:
	OutStreamEncrypted(int heist);

	OutStreamEncrypted& operator<<(const char *str);
	OutStreamEncrypted& operator<<(int num);
	OutStreamEncrypted& operator<<(void(*pf)(FILE*));
private:
	void encrypt(char** buffer, const char* str) const;
};